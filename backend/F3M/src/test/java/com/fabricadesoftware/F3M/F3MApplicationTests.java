package com.fabricadesoftware.F3M;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fabricadesoftware.F3M.enity.CadastroUsuario;

@SpringBootTest
class F3MApplicationTests {

	@Test
	void testNome() {
		CadastroUsuario cadast = new CadastroUsuario();
		cadast.setNomeUsuario("Fernando");
		String UserCadast = cadast.getNomeUsuario();
		String valorEsperado = "Fernando";
		assertEquals(valorEsperado, UserCadast);
		System.out.println("Deu bom?");
	}

	@Test
	void testEmail() {
		CadastroUsuario email = new CadastroUsuario();
		email.setEmailUsuario("cadastro@email.com");;
		String EmailCadast = email.getEmailUsuario();
		String valorEsperado = "cadastro@email.com";
		assertEquals(valorEsperado, EmailCadast);
		System.out.println("Email recebe as informações!");
	}
	
	@Test
	void testFuncao() {
		CadastroUsuario function = new CadastroUsuario();
		function.setFuncaoUsuario("Supervisor");
		String FuncaoCadast = function.getFuncaoUsuario();
		String valorEsperado = "Supervisor";
		assertEquals(valorEsperado, FuncaoCadast);
		System.out.println("Função do Usuario cadastrado!");
	}

}

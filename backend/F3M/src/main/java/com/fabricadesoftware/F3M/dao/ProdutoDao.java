package com.fabricadesoftware.F3M.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fabricadesoftware.F3M.enity.Produto;

@Repository
public interface ProdutoDao extends JpaRepository<Produto, Long> {

    Produto findProdutoByCodigoBarra(String codigoBarra);
}
package com.fabricadesoftware.F3M.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fabricadesoftware.F3M.dao.cadastroUsuarioDao;
import com.fabricadesoftware.F3M.enity.CadastroUsuario;



@CrossOrigin("*")
@RestController
@RequestMapping("/usuarios")
public class cadastroUsuarioRest {

	@Autowired
	private cadastroUsuarioDao cadastroUsuarioDao;
	
	@GetMapping //expoe o metodo GET
	public List<CadastroUsuario> get(){
		return cadastroUsuarioDao.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<CadastroUsuario> getById(@PathVariable Long id) {
		return cadastroUsuarioDao.findById(id);
	}
	@PostMapping
	public void post(@RequestBody CadastroUsuario cadastro) {
		cadastroUsuarioDao.save(cadastro);
	}
	
	@PutMapping
	public void put(@RequestBody CadastroUsuario cadastro) {
		cadastroUsuarioDao.save(cadastro);
	}
	

	@PostMapping("/user")
	public  ResponseEntity<CadastroUsuario> getByEmail(@RequestBody CadastroUsuario cadastro) {
		//variavel usuario contem a senha certa
		//a varivael cadastrro contem a senha que ele enviou por parametro
		Optional<CadastroUsuario> usuario =  Optional.ofNullable(cadastroUsuarioDao.findByEmailUsuario(cadastro.getEmailUsuario()));
		if(!usuario.get().getSenha().equals(cadastro.getSenha())) {
			cadastro.setNomeUsuario("acesso negado");
			return new ResponseEntity<>(cadastro, HttpStatus.FORBIDDEN);
		}
			return new ResponseEntity<>(cadastro, HttpStatus.ACCEPTED);
	}
	
	
	@DeleteMapping("/{id}") //serve para DELETAR via ID
	public void delete (@PathVariable("id") Long id) {
		 cadastroUsuarioDao.deleteById(id);
	}
}


package com.fabricadesoftware.F3M;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication // faz a classe se comportar como start do projeto
@EnableSwagger2
public class F3MApplication {

	public static void main(String[] args) {
		SpringApplication.run(F3MApplication.class, args);
	}

}

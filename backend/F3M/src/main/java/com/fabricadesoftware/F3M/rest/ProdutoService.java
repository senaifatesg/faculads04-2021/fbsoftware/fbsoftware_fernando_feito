package com.fabricadesoftware.F3M.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fabricadesoftware.F3M.enity.Produto;
import com.fabricadesoftware.F3M.dao.ProdutoDao;


@Service
public class ProdutoService {

	@Autowired
	private ProdutoDao produtoRepository;

	/**
	 * Monta Lista com todos os produtos
	 * 
	 * @return
	 */
	public List<Produto> getProduto() {
		return produtoRepository.findAll();
	}

}

package com.fabricadesoftware.F3M.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fabricadesoftware.F3M.enity.CadastroUsuario;

@Repository
public interface cadastroUsuarioDao extends JpaRepository<CadastroUsuario, Long>{
	
	CadastroUsuario findByEmailUsuario(String email);

}

import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Usuario } from '../interfaces/usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl: String = 'http://localhost:8080'

  mostrarMenuEmitter = new EventEmitter<boolean>();
  

  constructor(private http: HttpClient, private rout: Router) {
  }


  fazerlogin(usuario: Usuario): Observable<Usuario> {
    const url = this.baseUrl + '/usuarios/user'
    return this.http.post<Usuario>(url, usuario)
     
  }

  logou(){
    this.rout.navigate(['pdv']);
      }
}

import { Component, OnInit } from '@angular/core';
import { CadUsuarioService } from '../services/cad-usuario.service';
import { Usuario } from '../interfaces/usuario';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login: String = "";
  password: String = "";

  usuario: Usuario = {
    id: "",

  }

  constructor(private authService: AuthService) { }

  ngOnInit(): void {

  }

  fazerLogin() {
    this.usuario.emailUsuario = this.login;
    this.usuario.senha = this.password;
    
    this.authService.fazerlogin(this.usuario).subscribe(pao => {
      //sessionStorage.setItem("logado", "true")
        alert("Parabens, ja pode acessar o sistema")

        this.authService.mostrarMenuEmitter.emit(true);

     this.authService.logou();
    }, err => {

      alert("ACESSO NEGADO < PREENCHA OS CAMPOS")
      this.authService.mostrarMenuEmitter.emit(false);
      
      
    })
    console.log(this.login + '' + this.password)

  }

  
}

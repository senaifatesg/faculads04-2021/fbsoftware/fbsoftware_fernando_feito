import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CadastroDeProdutosComponent } from './cadastro-de-produtos/cadastro-de-produtos.component';
import { CadastroDeUsuarioComponent } from './cadastro-de-usuario/cadastro-de-usuario.component';
import { ConsultaCaixaComponent } from './consulta-caixa/consulta-caixa.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';
import { ListarUsuarioComponent } from './listar-usuario/listar-usuario.component';
import { LoginComponent } from './login/login.component';
import { PDVComponent } from './pdv/pdv.component';

const routes: Routes = [
  { path: '', pathMatch: "full",redirectTo: "login"},
  { path: 'home', component: AppComponent},
  { path: 'login', component: LoginComponent},
  { path: 'cadastroProdutos', component: CadastroDeProdutosComponent},
  { path: 'cadastroUsuario', component: CadastroDeUsuarioComponent},
  { path: 'consultaCaixa', component: ConsultaCaixaComponent},
  { path: 'listarUsuario', component: ListarUsuarioComponent},
  { path: 'pdv', component: PDVComponent},
  { path: 'editarUsuario/:id', component: EditarUsuarioComponent},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

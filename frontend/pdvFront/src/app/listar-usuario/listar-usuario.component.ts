import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from '../interfaces/usuario';
import { CadUsuarioService } from '../services/cad-usuario.service';

@Component({
  selector: 'app-listar-usuario',
  templateUrl: './listar-usuario.component.html',
  styleUrls: ['./listar-usuario.component.css']
})
export class ListarUsuarioComponent implements OnInit {

  usuarios:Usuario[] = [];
  idDoUsuario!: string;
  id!: number;
  cnfSenha: string = "";
  usuario: Usuario = {
    id:""

  }

  constructor(private service:CadUsuarioService, private router: Router, private rout: ActivatedRoute) { }

  ngOnInit(): void {
    this.listar();

  }

  public listar(){
    this.service.getAllUsers().subscribe(resposta => {
      this.usuarios = resposta;
    });
  }

  irParaTelaDeEditar(any : String){
this.router.navigate(['editarUsuario/'+ any]);
  }

  deleteUsuario(id: any){

    this.service.deletar(id).subscribe(resposta => {
      alert('Foi Removido com sucesso!') 
      this.atualizarTable();
    })
  }

  atualizarTable(){
    this.listar()
    for (let i = 0; i < this.usuarios.length; ++i) {
      if (this.usuarios[i]) {
        this.usuarios.splice(i, 1);
      }
    }
  }


}



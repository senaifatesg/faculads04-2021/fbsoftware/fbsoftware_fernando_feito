import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Produto } from '../interfaces/pdv.model';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  baseUrl: String = 'http://localhost:8080'

  private readonly PATH: string = 'produto';

  constructor(private http: HttpClient) {
  }

  findAllProdutosById(): Observable<Produto[]> {
    const url = this.baseUrl + '/produto'
    return this.http.get<Produto[]>(url)
  }
  findAllProdutosByCod(cod: String): Observable<Produto> {
    const url = `${this.baseUrl}/produto/codigo/${cod}`;
    return this.http.get<Produto>(url)
  }

  listarProduto(): Observable<any>{
    return this.http.get<Produto[]>(environment.baseUrl + this.PATH);
}  
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Usuario } from '../interfaces/usuario';

@Injectable({
  providedIn: 'root'
})
export class CadUsuarioService {

  baseUrl: String = "http://localhost:8080"


  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<Usuario[]> {
    const url = `${this.baseUrl}/usuarios`;
    return this.http.get<Usuario[]>(url)
  }

  getById(id: any): Observable<Usuario> {
    const url = `${this.baseUrl}/usuarios/${id}`;
    return this.http.get<Usuario>(url)
  }


  Save(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(`${this.baseUrl}/usuarios`, usuario)
  }

  atualizar(usuario: Usuario): Observable<Usuario> {
    const url = `${this.baseUrl}/usuarios`;
    return this.http.put<Usuario>(url, usuario);
  }

  deletar(id: string): Observable<any> {
    const url = `${this.baseUrl}/usuarios/${id}`;
    return this.http.delete(url)
  }

}

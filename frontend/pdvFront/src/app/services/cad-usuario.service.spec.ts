import { TestBed } from '@angular/core/testing';

import { CadUsuarioService } from './cad-usuario.service';

describe('CadUsuarioService', () => {
  let service: CadUsuarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CadUsuarioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

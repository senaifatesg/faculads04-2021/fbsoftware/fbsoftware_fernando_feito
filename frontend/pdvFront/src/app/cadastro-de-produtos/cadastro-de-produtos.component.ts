import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cadastro-de-produtos',
  templateUrl: './cadastro-de-produtos.component.html',
  styleUrls: ['./cadastro-de-produtos.component.css']
})
export class CadastroDeProdutosComponent implements OnInit {

  logado = false;
  constructor() { }

  ngOnInit(): void {
    if (sessionStorage.getItem("logado") != null) {
      this.logado = true;
    }
  }
}

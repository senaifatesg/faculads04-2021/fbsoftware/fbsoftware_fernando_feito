import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadastroDeUsuarioComponent } from './cadastro-de-usuario/cadastro-de-usuario.component';
import { CadastroDeProdutosComponent } from './cadastro-de-produtos/cadastro-de-produtos.component';
import { PDVComponent } from './pdv/pdv.component';
import { ConsultaCaixaComponent } from './consulta-caixa/consulta-caixa.component';
import { LoginComponent } from './login/login.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ListarUsuarioComponent } from './listar-usuario/listar-usuario.component';

import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';


@NgModule({
  declarations: [
    AppComponent,
    CadastroDeUsuarioComponent,
    CadastroDeProdutosComponent,
    PDVComponent,
    ConsultaCaixaComponent,
    LoginComponent,
    ListarUsuarioComponent,
    EditarUsuarioComponent,
  ],
  imports: [
    BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatSelectModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 


}

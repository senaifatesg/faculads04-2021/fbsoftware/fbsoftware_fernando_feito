import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Usuario } from '../interfaces/usuario';
import { CadUsuarioService } from '../services/cad-usuario.service';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {

  idDoUsuario!: string;
  cnfSenha: string = "";
  usuario: Usuario = {
    id:""

  }
  constructor(private service: CadUsuarioService, private route: ActivatedRoute, private _route: Router) { }

  ngOnInit(): void {
    this.idDoUsuario = this.route.snapshot.paramMap.get("id")!;
    this.service.getById(this.idDoUsuario).subscribe(resposta => {
      this.usuario = resposta;
    })
  }

  alteraUsuario(){
    this.usuario.id = this.idDoUsuario;
    console.log(this.usuario)
    if(this.usuario.senha != this.cnfSenha){
      alert("Voce é tao burro assim?")
    }else
    this.service.atualizar(this.usuario).subscribe(resposta => {
      alert("Usuario alterado com exito, parabens")
      this.cancel();
    }, error => {
      alert("erro ao alterar usuario nesse carai")
    });
  }

  cancel(){
    this._route.navigate(['listarUsuario']);
      }

}

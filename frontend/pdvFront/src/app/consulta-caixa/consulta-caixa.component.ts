import { Component, OnInit } from '@angular/core';
import { ProdutoService } from '../services/produto.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Produto} from '../interfaces/pdv.model';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-consulta-caixa',
  templateUrl: './consulta-caixa.component.html',
  styleUrls: ['./consulta-caixa.component.css']
})
export class ConsultaCaixaComponent implements OnInit {


  produtoList: Produto[] = [];
  logado = false;
  constructor(private service: ProdutoService ) { }

  ngOnInit(): void {
    this.listarProdutos();

    if( sessionStorage.getItem("logado") != null){
      this.logado = true;
    }
  }

  listarProdutos(){
    this.service.listarProduto().subscribe(
      data => {
        this.produtoList = data;
      },
      err => {
        let msg: string = "Não foi possivel obter produtos";
        alert(msg);
      }
    )
  }

}

export interface Usuario {
    id:string;
    emailUsuario?: String;
    nomeUsuario?: String;
    sobrenomeUsuario?: String;
    funcaoUsuario?: String;
    senha?: String;
}